package com.github.glukos.orderbook.utility;

import com.github.glukos.orderbook.OrderBook;
import com.github.glukos.orderbook.OrderBookImpl;
import com.github.glukos.orderbook.Trade;
import com.github.glukos.orderbook.order.Order;
import com.github.glukos.orderbook.time.FakeIncrementingTimeProvider;
import com.github.glukos.orderbook.time.TimeProvider;

import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

/**
 * Order book command line utility.
 * Reads order messages from standard input.
 * Prints trade messages and current order book state to standard output after every order message read.
 */
public class OrderBookUtility {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PrintWriter pw = new PrintWriter(System.out);

        TimeProvider timeProvider = new FakeIncrementingTimeProvider();

        OrderParser orderParser = new OrderParser(timeProvider);

        OrderBook orderBook = new OrderBookImpl();

        while (sc.hasNextLine()) {
            String message = sc.nextLine();

            if (message.trim().isEmpty())
                continue;

            Order nextOrder = orderParser.parseOrder(message);

            orderBook.submitOrder(nextOrder);

            List<Trade> trades = orderBook.processOrders();

            for (Trade trade : trades)
                pw.println(OutputUtil.printTradeMessage(trade));

            pw.println(OutputUtil.printOrderBook(orderBook));

            pw.flush();
        }
    }
}
