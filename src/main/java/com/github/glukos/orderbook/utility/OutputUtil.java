package com.github.glukos.orderbook.utility;

import com.github.glukos.orderbook.OrderBook;
import com.github.glukos.orderbook.Trade;
import com.github.glukos.orderbook.order.Order;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

/**
 * Helpful utility methods to generate output for {@link OrderBookUtility}.
 */
public class OutputUtil {
    private static final String HEADER =
        "+-----------------------------------------------------------------+\n" +
            "| BUY                            | SELL                           |\n" +
            "| Id       | Volume      | Price | Price | Volume      | Id       |\n" +
            "+----------+-------------+-------+-------+-------------+----------+\n";

    private static final String FOOTER =
        "+-----------------------------------------------------------------+\n";

    private static final DecimalFormat CURRENCY_FORMAT = new DecimalFormat("#,###");

    static {
        CURRENCY_FORMAT.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.UK));
    }

    private static final String COLUMN_DELIMITER = "|";

    private static final int ID_COLUMN_WIDTH = 10;
    private static final int VOLUME_COLUMN_WIDTH = 13;
    private static final int PRICE_COLUMN_WIDTH = 7;

    public static String printOrderBook(OrderBook orderBook) {
        List<Order> buyOrders = orderBook.getBuyOrders();
        List<Order> sellOrders = orderBook.getSellOrders();

        int lines = Math.max(buyOrders.size(), sellOrders.size());

        StringBuilder output = new StringBuilder().append(HEADER);

        for (int i = 0; i < lines; i++) {
            Order buyOrder = i < buyOrders.size() ? buyOrders.get(i) : null;
            Order sellOrder = i < sellOrders.size() ? sellOrders.get(i) : null;

            output.append(orderBookLine(buyOrder, sellOrder)).append("\n");
        }

        output.append(FOOTER);

        return output.toString();
    }

    static String printTradeMessage(Trade trade) {
        return trade.getBuyOrderId() + "," + trade.getSellOrderId() + "," + trade.getPrice() + "," +
            trade.getQuantity();
    }

    private static String orderBookLine(Order buyOrder, Order sellOrder) {
        return COLUMN_DELIMITER +
            formattedId(buyOrder) + COLUMN_DELIMITER +
            formattedVolume(buyOrder) + COLUMN_DELIMITER +
            formattedPrice(buyOrder) + COLUMN_DELIMITER +
            formattedPrice(sellOrder) + COLUMN_DELIMITER +
            formattedVolume(sellOrder) + COLUMN_DELIMITER +
            formattedId(sellOrder) + COLUMN_DELIMITER;
    }

    private static String formattedId(Order order) {
        return fixedLength(order == null ? "" : Integer.toString(order.getId()), ID_COLUMN_WIDTH);
    }

    private static String formattedVolume(Order order) {
        return fixedLength(order == null ? "" : CURRENCY_FORMAT.format(order.getQuantity()), VOLUME_COLUMN_WIDTH);
    }

    private static String formattedPrice(Order order) {
        return fixedLength(order == null ? "" : CURRENCY_FORMAT.format(order.getPrice()), PRICE_COLUMN_WIDTH);
    }

    private static String fixedLength(String string, int length) {
        return String.format("%1$" + length + "s", string);
    }
}
