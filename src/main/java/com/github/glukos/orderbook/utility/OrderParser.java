package com.github.glukos.orderbook.utility;

import com.github.glukos.orderbook.order.IcebergOrder;
import com.github.glukos.orderbook.order.LimitOrder;
import com.github.glukos.orderbook.order.Order;
import com.github.glukos.orderbook.order.OrderSide;
import com.github.glukos.orderbook.time.TimeProvider;

/**
 * Creates order instances from input of {@link OrderBookUtility}.
 */
class OrderParser {
    private final TimeProvider timeProvider;

    OrderParser(TimeProvider timeProvider) {
        this.timeProvider = timeProvider;
    }

    Order parseOrder(String orderMessage) {
        String[] parts = orderMessage.trim().split(",");

        if (parts.length < 4 || parts.length > 5)
            throw new IllegalArgumentException("Illegal order message: " + orderMessage);

        OrderSide side = "B".equals(parts[0]) ? OrderSide.BUY : OrderSide.SELL;
        int orderId = Integer.valueOf(parts[1]);
        int price = Integer.valueOf(parts[2]);
        int quantity = Integer.valueOf(parts[3]);

        if (parts.length == 5) {
            int peak = Integer.valueOf(parts[4]);

            return new IcebergOrder(orderId, timeProvider.getTimestamp(), side, price, quantity, peak, timeProvider);
        } else {
            return new LimitOrder(orderId, timeProvider.getTimestamp(), side, price, quantity);
        }
    }
}
