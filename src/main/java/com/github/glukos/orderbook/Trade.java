package com.github.glukos.orderbook;

public class Trade {
    private final int buyOrderId;

    private final int sellOrderId;

    private final int price;

    private final int quantity;

    Trade(int buyOrderId, int sellOrderId, int price, int quantity) {
        this.buyOrderId = buyOrderId;
        this.sellOrderId = sellOrderId;
        this.price = price;
        this.quantity = quantity;
    }

    public int getBuyOrderId() {
        return buyOrderId;
    }

    public int getSellOrderId() {
        return sellOrderId;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    Key getKey() {
        return new Key(buyOrderId, sellOrderId);
    }

    public static class Key {
        private final int buyOrderId;

        private final int sellOrderId;

        Key(int buyOrderId, int sellOrderId) {
            this.buyOrderId = buyOrderId;
            this.sellOrderId = sellOrderId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            return buyOrderId == key.buyOrderId && sellOrderId == key.sellOrderId;
        }

        @Override
        public int hashCode() {
            int result = buyOrderId;
            result = 31 * result + sellOrderId;
            return result;
        }
    }
}
