package com.github.glukos.orderbook.time;

/**
 * Responsible for providing timestamps for order trading.
 */
public interface TimeProvider {
    long getTimestamp();
}
