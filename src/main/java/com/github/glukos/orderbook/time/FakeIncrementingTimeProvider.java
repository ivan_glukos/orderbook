package com.github.glukos.orderbook.time;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Time provider that just increments time by 1 on each request.
 */
public class FakeIncrementingTimeProvider implements TimeProvider {
    private final AtomicLong counter = new AtomicLong(0);

    @Override
    public long getTimestamp() {
        return counter.incrementAndGet();
    }
}
