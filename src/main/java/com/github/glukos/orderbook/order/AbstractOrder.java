package com.github.glukos.orderbook.order;

import com.github.glukos.orderbook.OrderBook;

public abstract class AbstractOrder implements Order {
    private final int id;

    private final long timestamp;

    private final OrderSide side;

    private final int price;

    int quantity;

    AbstractOrder(int id, long timestamp, OrderSide side, int price, int quantity) {
        this.id = id;
        this.timestamp = timestamp;
        this.side = side;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getQuantity() {
        return quantity;
    }

    public OrderSide getSide() {
        return side;
    }

    public int getPrice() {
        return price;
    }

    public void fillOrder(OrderBook book, Order matchedOrder, int quantity) {
        if (quantity > this.quantity)
            throw new IllegalArgumentException("Can't fill more than current order quantity: " + this.quantity);

        this.quantity -= quantity;
    }
}
