package com.github.glukos.orderbook.order;

public class LimitOrder extends AbstractOrder {
    public LimitOrder(int id, long timestamp, OrderSide side, int price, int quantity) {
        super(id, timestamp, side, price, quantity);
    }
}
