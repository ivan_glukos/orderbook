package com.github.glukos.orderbook.order;

import com.github.glukos.orderbook.OrderBook;

/**
 * Encapsulates order - investor's instructions to purchase or sell a security.
 */
public interface Order {
    int getId();

    long getTimestamp();

    int getQuantity();

    OrderSide getSide();

    int getPrice();

    /**
     * Callback for executing order type specific actions, if there are any.
     * Should be called on order fill, both partial and full.
     *
     * @param book Order book in which order is being filled.
     * @param matchedOrder Matched order of opposite side.
     * @param quantity Fill quantity.
     */
    void fillOrder(OrderBook book, Order matchedOrder, int quantity);
}
