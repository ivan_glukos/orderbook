package com.github.glukos.orderbook.order;

import com.github.glukos.orderbook.OrderBook;
import com.github.glukos.orderbook.time.TimeProvider;

public class IcebergOrder extends AbstractOrder {
    private final TimeProvider timeProvider;

    private long initialTimestamp;

    private int fullQuantity;

    private int peakSize;

    public IcebergOrder(int id, long timestamp, OrderSide side, int price, int fullQuantity,
                        int peakSize, TimeProvider timeProvider) {
        super(id, timestamp, side, price, peakSize);

        if (peakSize > fullQuantity)
            throw new IllegalArgumentException("peakSize should be less or equal than fullQuantity");

        this.fullQuantity = fullQuantity;
        this.peakSize = peakSize;
        this.timeProvider = timeProvider;
        this.initialTimestamp = timestamp;
    }

    private IcebergOrder(int id, long initialTimestamp, long timestamp, OrderSide side, int price, int fullQuantity,
                         int peakSize, TimeProvider timeProvider) {
        this(id, timestamp, side, price, fullQuantity, peakSize, timeProvider);

        this.initialTimestamp = initialTimestamp;
    }

    @Override
    public void fillOrder(OrderBook book, Order matchedOrder, int quantity) {
        super.fillOrder(book, matchedOrder, quantity);

        if (getQuantity() == 0) {
            // Complete fill case.
            if (fullQuantity == peakSize)
                return;

            int newFullQuantity = fullQuantity - peakSize;
            int newPeakSize = Math.min(peakSize, newFullQuantity);

            book.submitOrder(new IcebergOrder(getId(), initialTimestamp, timeProvider.getTimestamp(), getSide(),
                getPrice(), newFullQuantity, newPeakSize, timeProvider));
        } else {
            // Partial fill case.
            boolean isAggressiveEntry = initialTimestamp > matchedOrder.getTimestamp();

            if (isAggressiveEntry) {
                int newFullQuantity = fullQuantity - peakSize + getQuantity();
                int newPeakSize = Math.min(newFullQuantity, peakSize);

                fullQuantity = newFullQuantity;
                peakSize = newPeakSize;
                this.quantity = peakSize;
            }
        }
    }
}
