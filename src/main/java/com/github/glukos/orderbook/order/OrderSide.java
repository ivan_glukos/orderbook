package com.github.glukos.orderbook.order;

public enum OrderSide {
    BUY,
    SELL
}
