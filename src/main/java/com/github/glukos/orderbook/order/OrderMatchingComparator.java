package com.github.glukos.orderbook.order;

import java.util.Comparator;

/**
 * Comparator for order matching. Resolves which order in book will be matched and filled first.
 */
public class OrderMatchingComparator implements Comparator<Order> {
    public static OrderMatchingComparator INSTANCE = new OrderMatchingComparator();

    @Override
    public int compare(Order o1, Order o2) {
        if (o1.getSide() != o2.getSide())
            throw new IllegalStateException("Orders with different sides are not comparable");

        int cmp = Integer.compare(o1.getPrice(), o2.getPrice()) * (o1.getSide() == OrderSide.SELL ? 1 : -1);
        if (cmp != 0)
            return cmp;

        return Long.compare(o1.getTimestamp(), o2.getTimestamp());
    }
}
