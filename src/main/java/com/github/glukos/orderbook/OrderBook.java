package com.github.glukos.orderbook;

import com.github.glukos.orderbook.order.Order;

import java.util.List;

/**
 * Interface for order book. Responsible for storing all submitted orders and matching them against each other.
 */
public interface OrderBook {
    void submitOrder(Order order);

    /**
     * Executes order matching algorithm.
     *
     * @return List of resulting trades in case any orders were successfully matched, empty list otherwise.
     */
    List<Trade> processOrders();

    List<Order> getBuyOrders();

    List<Order> getSellOrders();
}
