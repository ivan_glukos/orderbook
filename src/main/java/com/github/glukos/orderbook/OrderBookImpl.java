package com.github.glukos.orderbook;

import com.github.glukos.orderbook.order.Order;
import com.github.glukos.orderbook.order.OrderMatchingComparator;
import com.github.glukos.orderbook.order.OrderSide;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class OrderBookImpl implements OrderBook {
    private final TreeSet<Order> buyOrders;

    private final TreeSet<Order> sellOrders;

    public OrderBookImpl() {
        this.buyOrders = new TreeSet<>(OrderMatchingComparator.INSTANCE);
        this.sellOrders = new TreeSet<>(OrderMatchingComparator.INSTANCE);
    }

    @Override
    public void submitOrder(Order order) {
        if (order.getSide() == OrderSide.BUY) {
            buyOrders.add(order);
        } else {
            sellOrders.add(order);
        }
    }

    @Override
    public List<Trade> processOrders() {
        List<Trade> resultTrades = new ArrayList<>();

        while (true) {
            if (buyOrders.isEmpty() || sellOrders.isEmpty())
                return mergeTrades(resultTrades);

            Order buyOrder = buyOrders.first();
            Order sellOrder = sellOrders.first();

            if (buyOrder.getPrice() < sellOrder.getPrice())
                return mergeTrades(resultTrades);

            Order firstOrder = buyOrder.getTimestamp() < sellOrder.getTimestamp() ? buyOrder : sellOrder;

            // If prices of matched orders are not equal, first price is used for trade (last is considered as At Best)
            int price = firstOrder.getPrice();

            int quantity = Math.min(buyOrder.getQuantity(), sellOrder.getQuantity());

            resultTrades.add(new Trade(buyOrder.getId(), sellOrder.getId(), price, quantity));

            buyOrder.fillOrder(this, sellOrder, quantity);
            sellOrder.fillOrder(this, buyOrder, quantity);

            if (buyOrder.getQuantity() == 0)
                buyOrders.pollFirst();

            if (sellOrder.getQuantity() == 0)
                sellOrders.pollFirst();
        }
    }

    @Override
    public List<Order> getBuyOrders() {
        return new ArrayList<>(buyOrders);
    }

    @Override
    public List<Order> getSellOrders() {
        return new ArrayList<>(sellOrders);
    }

    private static List<Trade> mergeTrades(List<Trade> trades) {
        final BinaryOperator<Trade> quantitySumOperator = (left, right) -> new Trade(right.getBuyOrderId(),
            right.getSellOrderId(), right.getPrice(), left.getQuantity() + right.getQuantity());

        final Trade zeroTrade = new Trade(0, 0, 0, 0);

        Map<Trade.Key, Trade> mergedTrades = trades.stream().collect(Collectors.groupingBy(
            Trade::getKey,
            Collectors.reducing(zeroTrade, quantitySumOperator)
        ));

        return new ArrayList<>(mergedTrades.values());
    }
}
