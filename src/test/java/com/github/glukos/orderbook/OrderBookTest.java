package com.github.glukos.orderbook;


import com.github.glukos.orderbook.order.IcebergOrder;
import com.github.glukos.orderbook.order.LimitOrder;
import com.github.glukos.orderbook.order.Order;
import com.github.glukos.orderbook.order.OrderSide;
import com.github.glukos.orderbook.time.FakeIncrementingTimeProvider;
import com.github.glukos.orderbook.time.TimeProvider;
import com.github.glukos.orderbook.utility.OutputUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class OrderBookTest {
    private OrderBook orderBook;

    private TimeProvider timeProvider;

    private AtomicInteger latestOrderId;

    @Before
    public void setUp() {
        orderBook = new OrderBookImpl();

        timeProvider = new FakeIncrementingTimeProvider();

        latestOrderId = new AtomicInteger(100);
    }

    @Test
    public void testLimitOrdersNoMatching() {
        submitLimitOrder(OrderSide.BUY, 99, 10000);
        submitLimitOrder(OrderSide.BUY, 98, 20000);
        submitLimitOrder(OrderSide.BUY, 97, 30000);

        submitLimitOrder(OrderSide.SELL, 103, 30000);
        submitLimitOrder(OrderSide.SELL, 102, 20000);
        submitLimitOrder(OrderSide.SELL, 101, 10000);

        assertEquals(0, orderBook.processOrders().size());

        assertEquals(3, orderBook.getBuyOrders().size());
        assertEquals(3, orderBook.getSellOrders().size());
    }

    @Test
    public void testLimitSimplePartialFill() {
        Order buyMatched = submitLimitOrder(OrderSide.BUY, 99, 10000);
        submitLimitOrder(OrderSide.BUY, 98, 20000);

        submitLimitOrder(OrderSide.SELL, 102, 20000);
        submitLimitOrder(OrderSide.SELL, 101, 10000);

        Order sellMatched = submitLimitOrder(OrderSide.SELL, 99, 5000);

        List<Trade> trades = orderBook.processOrders();

        assertEquals(2, orderBook.getBuyOrders().size());
        assertEquals(2, orderBook.getSellOrders().size());

        assertEquals(1, trades.size());
        assertEquals(new Trade.Key(buyMatched.getId(), sellMatched.getId()), trades.get(0).getKey());
        assertEquals(99, trades.get(0).getPrice());
        assertEquals(5000, trades.get(0).getQuantity());
    }

    @Test
    public void testLimitSimpleFullFill() {
        submitLimitOrder(OrderSide.BUY, 5099, 200);
        Order buyMatched = submitLimitOrder(OrderSide.BUY, 5100, 10000);

        submitLimitOrder(OrderSide.SELL, 5101, 200);
        Order sellMatched = submitLimitOrder(OrderSide.SELL, 5100, 10000);

        List<Trade> trades = orderBook.processOrders();

        assertEquals(1, orderBook.getBuyOrders().size());
        assertEquals(1, orderBook.getSellOrders().size());

        assertEquals(1, trades.size());
        assertEquals(new Trade.Key(buyMatched.getId(), sellMatched.getId()), trades.get(0).getKey());
        assertEquals(5100, trades.get(0).getPrice());
        assertEquals(10000, trades.get(0).getQuantity());
    }

    @Test
    public void testIcebergMultipleMatchesSingleTrade() {
        Order iceberg = submitIcebergOrder(OrderSide.BUY, 5100, 70000, 10000);
        submitLimitOrder(OrderSide.SELL, 5100, 16000);

        List<Trade> trades = orderBook.processOrders();

        assertEquals(1, orderBook.getBuyOrders().size());
        assertEquals(4000, orderBook.getBuyOrders().get(0).getQuantity());
        assertEquals(0, orderBook.getSellOrders().size());

        assertEquals(1, trades.size());
        assertEquals(16000, trades.get(0).getQuantity());
        assertEquals(iceberg.getId(), trades.get(0).getBuyOrderId());
    }

    /**
     * Case from chapter 4.2.3.1 of SETSmm and Iceberg Orders.
     * Test prints state of order book after every step.
     */
    @Test
    public void testIcebergOrderAggressiveEntry() {
        submitLimitOrder(OrderSide.SELL, 101, 20000);
        submitLimitOrder(OrderSide.BUY, 99, 50000);
        submitLimitOrder(OrderSide.SELL, 100, 10000);
        submitLimitOrder(OrderSide.SELL, 100, 7500);
        submitLimitOrder(OrderSide.BUY, 98, 25500);

        System.out.println(OutputUtil.printOrderBook(orderBook));

        submitIcebergOrder(OrderSide.BUY, 100, 100000, 10000);

        List<Trade> trades = orderBook.processOrders();
        System.out.println(OutputUtil.printOrderBook(orderBook));

        assertEquals(2, trades.size());
        assertEquals(17500, trades.stream().collect(Collectors.summarizingInt(Trade::getQuantity)).getSum());

        assertEquals(3, orderBook.getBuyOrders().size());
        assertEquals(10000, orderBook.getBuyOrders().get(0).getQuantity());

        assertEquals(1, orderBook.getSellOrders().size());
        assertEquals(20000, orderBook.getSellOrders().get(0).getQuantity());
    }

    /**
     * Case from chapter 4.2.3.2 of SETSmm and Iceberg Orders.
     * Test prints state of order book after every step.
     */
    @Test
    public void testIcebergOrderPassiveExecution() {
        submitLimitOrder(OrderSide.SELL, 101, 20000);
        submitLimitOrder(OrderSide.BUY, 99, 50000);
        submitLimitOrder(OrderSide.BUY, 98, 25500);
        Order icebergA = submitIcebergOrder(OrderSide.BUY, 100, 82500, 10000);

        submitLimitOrder(OrderSide.SELL, 100, 10000);

        List<Trade> trades = orderBook.processOrders();
        System.out.println(OutputUtil.printOrderBook(orderBook));

        assertEquals(1, trades.size());
        assertEquals(10000, trades.get(0).getQuantity());

        assertEquals(3, orderBook.getBuyOrders().size());
        assertEquals(10000, orderBook.getBuyOrders().get(0).getQuantity());

        submitLimitOrder(OrderSide.SELL, 100, 11000);

        trades = orderBook.processOrders();
        System.out.println(OutputUtil.printOrderBook(orderBook));

        assertEquals(1, trades.size());
        assertEquals(11000, trades.get(0).getQuantity());

        assertEquals(3, orderBook.getBuyOrders().size());
        assertEquals(9000, orderBook.getBuyOrders().get(0).getQuantity());

        Order icebergB = submitIcebergOrder(OrderSide.BUY, 100, 50000, 20000);
        System.out.println(OutputUtil.printOrderBook(orderBook));

        Order atBest = submitLimitOrder(OrderSide.SELL, 100, 35000);

        trades = orderBook.processOrders();
        System.out.println(OutputUtil.printOrderBook(orderBook));

        assertEquals(2, trades.size());
        assertEquals(35000, trades.stream().collect(Collectors.summarizingInt(Trade::getQuantity)).getSum());
        Map<Trade.Key, Trade> tradeMap = trades.stream().collect(Collectors.toMap(Trade::getKey, Function.identity()));

        Trade icebergATrade = tradeMap.get(new Trade.Key(icebergA.getId(), atBest.getId()));
        assertEquals(15000, icebergATrade.getQuantity());

        Trade icebergBTrade = tradeMap.get(new Trade.Key(icebergB.getId(), atBest.getId()));
        assertEquals(20000, icebergBTrade.getQuantity());

        assertEquals(4000, orderBook.getBuyOrders().get(0).getQuantity());
        assertEquals(icebergA.getId(), orderBook.getBuyOrders().get(0).getId());

        assertEquals(20000, orderBook.getBuyOrders().get(1).getQuantity());
        assertEquals(icebergB.getId(), orderBook.getBuyOrders().get(1).getId());
    }

    private Order submitLimitOrder(OrderSide side, int price, int quantity) {
        LimitOrder order = new LimitOrder(latestOrderId.incrementAndGet(), timeProvider.getTimestamp(),
            side, price, quantity);

        orderBook.submitOrder(order);

        return order;
    }

    private Order submitIcebergOrder(OrderSide side, int price, int quantity, int peak) {
        IcebergOrder order = new IcebergOrder(
            latestOrderId.incrementAndGet(), timeProvider.getTimestamp(), side, price, quantity, peak, timeProvider);

        orderBook.submitOrder(order);

        return order;
    }
}
